#!/usr/bin/env zsh
for design in superblue1 superblue3 superblue4 superblue5 superblue7 superblue10 superblue16 superblue18; do
  for algorithm in Tetris Abacus; do
    echo "${algorithm} ${design}"
    ${HOME}/builds/place-eval/Release/place-eval \
      --def ${HOME}/git-repos/rsyn-x/x/run/benchmarks/${design}/${design}.def:${HOME}/git-repos/rsyn-x/x/run/${algorithm}-result-${design}.def \
      --lef ${HOME}/git-repos/rsyn-x/x/run/benchmarks/${design}/${design}.lef
    echo ""
  done
done

