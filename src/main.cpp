#include "PlacementEvaluator.h"
#include "arg_parser.h"
#include "def_reader.h"
#include "drawer.h"
#include "lef_reader.h"

#include <memory>

int main(int argc, char *argv[]) {
  arg_parser parser;
  int parse_result = parser.parse_options(argc, argv);
  if (parse_result != 0) {
    return parse_result;
  }

  std::vector<std::string> const &defs = parser.get_defs();
  std::vector<std::string> const &lefs = parser.get_lefs();

  lef_data l_data;
  std::vector<def_data> d_data{2};

  {
    std::shared_ptr<lef_reader> l_reader = lef_reader::get_reader();
    for (std::string const &lef_filename : lefs) {
      int result = l_reader->read(lef_filename, &l_data);

      if (result != 0) {
        std::cerr << lef_filename << ": LEF parsing failed\n";
        return result;
      }
    }
  }

  {
    std::shared_ptr<def_reader> d_reader = def_reader::get_reader();
    for (size_t i = 0; i < defs.size(); ++i) {
      int result = d_reader->read(defs[i], &d_data[i]);
      if (result != 0) {
        std::cerr << defs[i] << ": DEF parsing failed\n";
        return result;
      }
    }
  }

  // if only one DEF file was given, check its legality
  if (defs.size() == 1) {

  }
  else if (defs.size() == 2) {
    // else evaluate the second placement compared to the first
    PlacementEvaluator ep(d_data[0], d_data[1]);
    if (!ep.check_legality()) {
      return 1;
    }

    std::cout << "LEGAL PLACEMENT!\n";
    return 0;
  }

  {
    QApplication app(argc, argv);

    Drawer dr(&l_data, &d_data[0]);
    dr.show();

    return QApplication::exec();
  }
  // if a second def was given, read it too, and draw displacements
}
