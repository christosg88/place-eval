#ifndef PLACEVIZ_ARG_PARSER_H
#define PLACEVIZ_ARG_PARSER_H

#include <boost/program_options.hpp>
#include <iostream>
#include <string>
#include <vector>

namespace po = boost::program_options;

class arg_parser {
private:
  po::options_description desc;
  std::vector<std::string> defs;
  std::vector<std::string> lefs;

public:
  arg_parser() : desc("Allowed options") {
    desc.add_options()
      ("help,h", "produce help message")
      ("def,d", po::value<std::string>(), "Semi-colon separated list of DEF files. e.g. init.def;final.def")
      ("lef,l", po::value<std::string>(), "Semi-colon separated list of LEF files. e.g. tech.lef;cells.lef")
    ;
  }

  int parse_options(int argc, char **argv);

  std::vector<std::string> const &get_defs() {
    return defs;
  }

  std::vector<std::string> const &get_lefs() {
    return lefs;
  }
};

#endif
