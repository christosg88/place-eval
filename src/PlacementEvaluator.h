#ifndef PLACE_EVAL_PLACEMENTEVALUATOR_H
#define PLACE_EVAL_PLACEMENTEVALUATOR_H

#include "def_reader.h"

class PlacementEvaluator {
private:
  def_data const *_initial_placement{nullptr};
  def_data const *_final_placement{nullptr};
  std::vector<defiComponent const *> _sorted_initial_components;
  std::vector<defiComponent const *> _sorted_initial_fixed_components;
  std::vector<defiComponent const *> _sorted_final_components;
  std::vector<defiComponent const *> _sorted_final_fixed_components;
  std::vector<defiRow const *> _sorted_initial_rows;
  std::vector<defiRow const *> _sorted_final_rows;


public:
  /**
   * Create a PlacementEvaluator to check the legality of a final placement.
   * @param final_placement The final placement's DEF data
   */
  PlacementEvaluator(def_data const &final_placement) : _final_placement(&final_placement) {}

  /**
   * Create a PlacementEvaluator to check the legality of a final placement and compare its cells displacement from
   * the initial placement.
   * @param initial_placement The initial placement's DEF data
   * @param final_placement The final placement's DEF data
   */
  PlacementEvaluator(def_data const &initial_placement, def_data const &final_placement);

  /**
   * Check if the final placement is legal.
   * @return true if the placement is legal, else false
   */
  bool check_legality();

  /**
   * Compute average and maximum displacement of cells from initial to final placement.
   */
  void compute_displacement();

private:
  /**
   * Check if all components that appear in the initial placement are still in the final placement, and if no extra
   * components were added to the final placement.
   * Side-effects: This method sorts the components of the initial and final placement in alphabetical order of their
   * IDs.
   * @return true if initial and final placements have the same components, else false.
   */
  bool check_component_equivalence();

  /**
   * Check if all rows that appear in the initial placement are still in the final placement, and if no extra rows
   * were added to the final placement.
   * Side-effects: This method sorts the rows of the initial and final placement in alphabetical order of their names.
   * @return true if initial and final placement have the same rows, else false.
   */
  bool check_row_equivalence();
};

#endif
