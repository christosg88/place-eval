#include "def_reader.h"

def_reader::def_reader() {
  // initialize the def session
  defrInitSession();

  defrSetDieAreaCbk(die_area_cbk);
  defrSetRegionCbk(region_cbk);
  defrSetRowCbk(row_cbk);
  defrSetComponentCbk(component_cbk);
}


def_reader::~def_reader() {
  defrClear();
}

int def_reader::read(std::string const &def_filename, def_data *d_data) {
  // open a FILE pointer to the def_filename file
  FILE *def_file = fopen(def_filename.c_str(), "r");
  if (def_file == nullptr) {
    std::cerr << "File not found: " << def_filename << std::endl;
    return -1;
  }

  defrSetUserData(static_cast<void *>(d_data));
  int status = defrRead(def_file, def_filename.c_str(), static_cast<void *>(d_data), 0);
  fclose(def_file);

  if (status != 0) {
    std::cerr << "DEF read failed\n";
  }

  return status;
}

int def_reader::die_area_cbk(defrCallbackType_e type, defiBox *die, defiUserData ud) {
  if (type != defrDieAreaCbkType || ud == nullptr) {
    std::cerr << "Error in die_area_cbk()\n";
    return -1;
  }

  def_data *d_data = static_cast<def_data *>(ud);
  d_data->_die = *die;

  return 0;
}

int def_reader::region_cbk(defrCallbackType_e type, defiRegion *region, defiUserData ud) {
  if (type != defrRegionCbkType || ud == nullptr) {
    std::cerr << "Error in region_cbk()\n";
    return -1;
  }

  def_data *d_data = static_cast<def_data *>(ud);
  d_data->_regions.emplace_back(*region);

  return 0;
}

int def_reader::row_cbk(defrCallbackType_e type, defiRow *row, defiUserData ud) {
  if (type != defrRowCbkType || ud == nullptr) {
    std::cerr << "Error in row_cbk()\n";
    return -1;
  }

  def_data *d_data = static_cast<def_data *>(ud);
  d_data->_rows.emplace_back(*row);

  return 0;
}

int def_reader::component_cbk(defrCallbackType_e type, defiComponent *comp, defiUserData ud) {
  if (type != defrComponentCbkType || ud == nullptr) {
    std::cerr << "Error in component_cbk()\n";
    return -1;
  }

  def_data *d_data = static_cast<def_data *>(ud);
  if (comp->isFixed() != 0) {
    d_data->_fixed_components.emplace_back(*comp);
  }
  else if (comp->isPlaced() != 0) {
    d_data->_placed_components.emplace_back(*comp);
  }
  else if (comp->isUnplaced() != 0) {
    d_data->_unplaced_components.emplace_back(*comp);
  }

  return 0;
}
