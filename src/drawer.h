#ifndef PLACEVIZ_DRAWER_H
#define PLACEVIZ_DRAWER_H

#include "defrData.hpp"

#include <QApplication>
#include <QWidget>

#include <algorithm>
#include <iostream>

class lef_data;

class def_data;

class QGraphicsScene;

class Drawer : public QWidget {
  Q_OBJECT

private:
  QGraphicsScene *_scene;

  lef_data const *_l_data = nullptr;
  def_data const *_d_data = nullptr;

public:
  Drawer(lef_data const *l_data, def_data const *d_data);

private:
  void draw_die();

  void draw_rows();

  void draw_regions();

  void draw_components();
};

#endif
