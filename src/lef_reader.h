#ifndef PLACEVIZ_LEF_READER_H
#define PLACEVIZ_LEF_READER_H

#include "lefrData.hpp"

#include <memory>
#include <unordered_map>

class lef_data {
public:
  // the sizes of sites and macros are in um. To transform to DBU multiply by _db_microns
  double _db_microns;
  std::unordered_map<std::string, lefiSite> _sites;
  std::unordered_map<std::string, lefiMacro> _macros;

  friend std::ostream &operator<<(std::ostream &os, lef_data const &l_data);
};

class lef_reader {
public:
  static std::shared_ptr<lef_reader> get_reader() {
    static std::shared_ptr<lef_reader> instance(new lef_reader());
    return instance;
  }

  ~lef_reader();

  static int read(std::string const &lef_filename, lef_data *l_data);

private:
  lef_reader();
  static int units_cbk(lefrCallbackType_e type, lefiUnits *units, lefiUserData ud);
  static int site_cbk(lefrCallbackType_e type, lefiSite *site, lefiUserData ud);
  static int macro_cbk(lefrCallbackType_e type, lefiMacro *macro, lefiUserData ud);
};

#endif
