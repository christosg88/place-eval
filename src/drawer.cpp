#include "def_reader.h"
#include "drawer.h"
#include "graphics_view.h"
#include "lef_reader.h"

#include <QGraphicsScene>
#include <QVBoxLayout>

Drawer::Drawer(lef_data const *l_data, def_data const *d_data)
    : QWidget(nullptr), _scene(new QGraphicsScene(this)), _l_data(l_data), _d_data(d_data) {
  // populate the scene
  draw_die();
  draw_rows();
  draw_components();

  // create the layout
  QVBoxLayout *layout = new QVBoxLayout();
  layout->setMargin(0);

  // create the view and add it to the layout
  GraphicsView *view = new GraphicsView(_scene);
  view->setRenderHint(QPainter::Antialiasing, false);
  view->setDragMode(QGraphicsView::RubberBandDrag);
  view->setOptimizationFlags(QGraphicsView::DontSavePainterState | QGraphicsView::DontAdjustForAntialiasing);
  view->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
  view->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
  layout->addWidget(view);

  // set the window's layout and maximize the window
  setLayout(layout);
  setWindowState(Qt::WindowMaximized);
}

void Drawer::draw_die() {
  Die *die = new Die(_d_data->_die);
  _scene->addItem(die);
  _scene->setSceneRect(die->boundingRect());
}

void Drawer::draw_rows() {
  for (defiRow row : _d_data->_rows) {
    lefiSite const &site = _l_data->_sites.find(row.macro())->second;
    row.setDo(row.xNum(), row.yNum(), row.xStep(), site.sizeY() * _l_data->_db_microns);
    _scene->addItem(new Row(row));
  }
}

void Drawer::draw_regions() {
  QPen pen;
  pen.setWidth(1);
  pen.setCosmetic(true); // don't resize the pen width when scaling

  for (defiRegion const &region : _d_data->_regions) {
    QColor color(100, 100, 100, 40);
    QBrush brush(color);

    std::cout << "region: " << region.name() << "\n";
    for (int i = 0, num_rectangles = region.numRectangles(); i < num_rectangles; ++i) {

      int x = region.xl(i);
      int y = region.yl(i);
      int w = region.xh(i) - x;
      int h = region.yh(i) - y;

      _scene->addRect(x, y, w, h, QPen(), brush);
    }
  }
}

void Drawer::draw_components() {
  QBrush fixed_cell_brush(QColor(0, 0, 0, 40));
  for (defiComponent const &comp : _d_data->_fixed_components) {
    lefiMacro const &lib_cell = _l_data->_macros.find(comp.name())->second;

    double x = comp.placementX();
    double y = comp.placementY();
    double w = lib_cell.sizeX() * _l_data->_db_microns;
    double h = lib_cell.sizeY() * _l_data->_db_microns;

    _scene->addRect(x, y, w, h, QPen(), fixed_cell_brush);
  }

  for (defiComponent const &comp : _d_data->_placed_components) {
    lefiMacro const &lib_cell = _l_data->_macros.find(comp.name())->second;

    double x = comp.placementX();
    double y = comp.placementY();
    double w = lib_cell.sizeX() * _l_data->_db_microns;
    double h = lib_cell.sizeY() * _l_data->_db_microns;

    _scene->addRect(x, y, w, h, QPen());
  }
}
