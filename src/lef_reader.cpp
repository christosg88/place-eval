#include "lef_reader.h"
#include "lefrReader.hpp"

#include <iostream>

std::ostream &operator<<(std::ostream &os, lef_data const &l_data) {
  os << std::fixed;
  for (auto const &[name, macro] : l_data._macros) {
    os << "name: " << name << "\n";
    auto site = l_data._sites.find(macro.siteName());
    os << "width: " << macro.sizeX() << " um == " << macro.sizeX() * l_data._db_microns << " DBU == " << macro.sizeX() / site->second.sizeX() << " sites\n";
    os << "height: " << macro.sizeY() << " um == " << macro.sizeY() * l_data._db_microns << " DBU == " << macro.sizeY() / site->second.sizeY() << " rows\n\n";
  }

  return os;
}

lef_reader::lef_reader() {
  // initialize the def session
  lefrInitSession();

  // set callbacks
  lefrSetUnitsCbk(units_cbk);
  lefrSetSiteCbk(site_cbk);
  lefrSetMacroCbk(macro_cbk);
}

lef_reader::~lef_reader() {
  lefrClear();
}

int lef_reader::read(std::string const &lef_filename, lef_data *l_data) {
  // open a FILE pointer to the lef_filename file
  FILE *lef_file = fopen(lef_filename.c_str(), "r");
  if (lef_file == nullptr) {
    std::cerr << "File not found: " << lef_filename << std::endl;
    return -1;
  }

  // set the pointer to user data to populate
  lefrSetUserData(static_cast<void *>(l_data));
  int status = lefrRead(lef_file, lef_filename.c_str(), static_cast<void *>(l_data));
  fclose(lef_file);

  if (status != 0) {
    std::cerr << "LEF read failed\n";
  }

  return status;
}

int lef_reader::units_cbk(lefrCallbackType_e type, lefiUnits *units, lefiUserData ud) {
  if (type != lefrUnitsCbkType || ud == nullptr) {
    std::cerr << "Error in units_cbk()\n";
    return -1;
  }

  lef_data *l_data = static_cast<lef_data *>(ud);
  l_data->_db_microns = units->databaseNumber();

  return 0;
}

int lef_reader::site_cbk(lefrCallbackType_e type, lefiSite *site, lefiUserData ud) {
  if (type != lefrSiteCbkType || ud == nullptr) {
    std::cerr << "Error in site_cbk()\n";
    return -1;
  }

  lef_data *l_data = static_cast<lef_data *>(ud);
  l_data->_sites.emplace(site->name(), *site);

  return 0;
}

int lef_reader::macro_cbk(lefrCallbackType_e type, lefiMacro *macro, lefiUserData ud) {
  if (type != lefrMacroCbkType || ud == nullptr) {
    std::cerr << "Error in macro_cbk()\n";
    return -1;
  }

  lef_data *l_data = static_cast<lef_data *>(ud);
  l_data->_macros.emplace(macro->name(), *macro);

  return 0;
}
