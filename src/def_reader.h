#ifndef PLACEVIZ_DEF_READER_H
#define PLACEVIZ_DEF_READER_H

#include "defrData.hpp"
#include "defrReader.hpp"

#include <QGraphicsItem>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

#include <cstdio>
#include <iostream>
#include <memory>
#include <string>

class Die : public QGraphicsItem {
private:
  defiBox _die;
  QRectF _box;

public:
  Die(defiBox const &die) :
      QGraphicsItem(),
      _die(die),
      _box(_die.xl(), _die.yl(), _die.xh() - _die.xl(), _die.yh() - _die.yl()) {}

  QRectF boundingRect() const override {
    return _box;
  }

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override {
    QPen pen = painter->pen();
    pen.setWidth(4);
    pen.setCosmetic(true); // don't resize the pen width when scaling
    painter->setPen(pen);

    painter->drawRect(_box);
  }
};

class Row : public QGraphicsItem {
private:
  defiRow _row;
  QRectF _box;

public:
  explicit Row(defiRow const &row) :
      QGraphicsItem(),
      _row(row),
      _box(row.x(), row.y(), row.xNum() * row.xStep(), row.yNum() * row.yStep()) {}

  QRectF boundingRect() const override {
    return _box;
  }

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override {
    const qreal lod = option->levelOfDetailFromTransform(painter->worldTransform());

    if (lod > 1e-2) {
      QPen pen = painter->pen();
      pen.setWidth(2);
      pen.setCosmetic(true); // don't resize the pen width when scaling
      painter->setPen(pen);

      painter->drawRect(_box);
    }
  }
};

class def_data {
public:
  defiBox _die;
  std::vector<defiRegion> _regions;
  std::vector<defiRow> _rows;
  std::vector<defiComponent> _fixed_components;
  std::vector<defiComponent> _placed_components;
  std::vector<defiComponent> _unplaced_components;
};

class def_reader {
public:
  static std::shared_ptr<def_reader> get_reader() {
    static std::shared_ptr<def_reader> instance(new def_reader());
    return instance;
  }

  ~def_reader();

  static int read(std::string const &def_filename, def_data *d_data);

private:
  def_reader();

  static int die_area_cbk(defrCallbackType_e type, defiBox *die, defiUserData ud);
  static int region_cbk(defrCallbackType_e type, defiRegion *region, defiUserData ud);
  static int row_cbk(defrCallbackType_e type, defiRow *row, defiUserData ud);
  static int component_cbk(defrCallbackType_e type, defiComponent *comp, defiUserData ud);
};

#endif
