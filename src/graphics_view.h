#ifndef PLACEVIZ_GRAPHICS_VIEW_H
#define PLACEVIZ_GRAPHICS_VIEW_H

#include <QGraphicsView>
#include <QWheelEvent>
#include <QtMath>

class GraphicsView : public QGraphicsView {
public:
  qreal zoom_level = 0;

  GraphicsView(QGraphicsScene *scene, QWidget *parent = nullptr) : QGraphicsView(scene, parent) {
  }

  void wheelEvent(QWheelEvent *e) override {
    if (e->modifiers() & Qt::ControlModifier) {
      if (e->angleDelta().y() > 0)
        setup_matrix(1);
      else
        setup_matrix(-1);
      e->accept();
    } else {
      QGraphicsView::wheelEvent(e);
    }
  }

  void keyPressEvent(QKeyEvent *e) override {
    if (e->modifiers() & Qt::ControlModifier) {
      if (e->key() == Qt::Key_Plus) {
        setup_matrix(1);
      }
      else if (e->key() == Qt::Key_Minus) {
        setup_matrix(-1);
      }
    }
    else {
      QGraphicsView::keyPressEvent(e);
    }
  }

  void setup_matrix(qreal zoom_diff) {
    zoom_level += zoom_diff;
    qreal scale = qPow(qreal(2), zoom_level / 10);

    QMatrix matrix;
    matrix.scale(scale, scale);
    setMatrix(matrix);
  }
};

#endif
