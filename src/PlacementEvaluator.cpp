#include "PlacementEvaluator.h"

#include <cmath>
#include <iomanip>
#include <iostream>
#include <set>

static void
copy_and_sort_components(std::vector<defiComponent const *> &copy, std::vector<defiComponent> const &original) {
  copy.reserve(original.size());
  for (defiComponent const &component : original) {
    copy.push_back(&component);
  }

  std::sort(copy.begin(), copy.end(), [](defiComponent const *first, defiComponent const *second) {
    return std::strcmp(first->id(), second->id()) < 0;
  });
}

static void
copy_and_sort_rows(std::vector<defiRow const *> &copy, std::vector<defiRow> const &original) {
  copy.reserve(original.size());
  for (defiRow const &component : original) {
    copy.push_back(&component);
  }

  std::sort(copy.begin(), copy.end(), [](defiRow const *first, defiRow const *second) {
    return first->y() < second->y();
  });
}

PlacementEvaluator::PlacementEvaluator(def_data const &initial_placement, def_data const &final_placement)
    : _initial_placement(&initial_placement), _final_placement(&final_placement) {
  copy_and_sort_components(_sorted_initial_components, _initial_placement->_placed_components);
  copy_and_sort_components(_sorted_initial_fixed_components, _initial_placement->_fixed_components);
  copy_and_sort_components(_sorted_final_components, _final_placement->_placed_components);
  copy_and_sort_components(_sorted_final_fixed_components, _final_placement->_fixed_components);
  copy_and_sort_rows(_sorted_initial_rows, _initial_placement->_rows);
  copy_and_sort_rows(_sorted_final_rows, _final_placement->_rows);
}

bool PlacementEvaluator::check_legality() {
  if (_initial_placement != nullptr) {
    if (!(check_component_equivalence() && check_row_equivalence())) {
      return false;
    }

    compute_displacement();
  }

  // TODO: add check for if cells are aligned to the rows, the sites, and that they don't overlap with each other

  return true;
}

void PlacementEvaluator::compute_displacement() {
  double total_displacement = 0.0;
  auto it1 = _sorted_initial_components.begin();
  auto it2 = _sorted_final_components.begin();
  auto end1 = _sorted_initial_components.end();

  // at this point we know that the two vectors have the same length, so we don't need to check for the end of both
  // of them
  for (; it1 != end1; ++it1, ++it2) {
    defiComponent const *initial = *it1;
    defiComponent const *final = *it2;

    double dispX = std::abs(final->placementX() - initial->placementX());
    double dispY = std::abs(final->placementY() - initial->placementY());
    double displacement = std::sqrt(dispX * dispX + dispY * dispY);

    total_displacement += displacement;
  }

  double average_displacement = total_displacement / _sorted_initial_components.size();
  double row_height = _sorted_final_rows[1]->y() - _sorted_final_rows[0]->y();

  std::cout << "[+] Total displacement "
            << std::fixed << std::setprecision(2) << total_displacement / row_height
            << " rows\n";
  std::cout << "[+] Average displacement "
            << std::fixed << std::setprecision(2) << average_displacement / row_height
            << " rows\n";
}

static std::pair<std::vector<defiComponent const *>, std::vector<defiComponent const *>>
compare_component_names(std::vector<defiComponent const *> &first, std::vector<defiComponent const *> &second) {
  std::pair<std::vector<defiComponent const *>, std::vector<defiComponent const *>> missing_components;

  auto it1 = first.begin();
  auto it2 = second.begin();
  while (it1 != first.end() && it2 != second.end()) {
    int result = std::strcmp((*it1)->id(), (*it2)->id());
    if (result < 0) {
      missing_components.second.push_back(*it1);
      ++it1;
    }
    else if (result > 0) {
      missing_components.first.push_back(*it2);
      ++it2;
    }
    else {
      ++it1; ++it2;
    }
  }
  while (it1 != first.end()) {
    missing_components.second.push_back(*it1);
    ++it1;
  }
  while (it2 != second.end()) {
    missing_components.first.push_back(*it2);
    ++it2;
  }

  return missing_components;
}

bool PlacementEvaluator::check_component_equivalence() {
  std::pair<std::vector<defiComponent const *>, std::vector<defiComponent const *>> missing_components_pair =
      compare_component_names(_sorted_initial_components, _sorted_final_components);

  bool missing_components = false;
  if (!missing_components_pair.first.empty()) {
    missing_components = true;
    for (defiComponent const *component : missing_components_pair.first) {
      std::cout << "[!] Placed component " << std::string(component->id()) << " is missing from the initial placement.\n";
    }
  }
  if (!missing_components_pair.second.empty()) {
    missing_components = true;
    for (defiComponent const *component : missing_components_pair.second) {
      std::cout << "[!] Placed component " << std::string(component->id()) << " is missing from the final placement.\n";
    }
  }

  missing_components_pair = compare_component_names(_sorted_initial_fixed_components, _sorted_final_fixed_components);
  if (!missing_components_pair.first.empty()) {
    missing_components = true;
    for (defiComponent const *component : missing_components_pair.first) {
      std::cout << "[!] Fixed component " << std::string(component->id()) << " is missing from the initial placement.\n";
    }
  }
  if (!missing_components_pair.second.empty()) {
    missing_components = true;
    for (defiComponent const *component : missing_components_pair.second) {
      std::cout << "[!] Fixed component " << std::string(component->id()) << " is missing from the final placement.\n";
    }
  }

  return !missing_components;
}

static std::pair<std::vector<defiRow const *>, std::vector<defiRow const *>>
compare_row_names(std::vector<defiRow const *> &first, std::vector<defiRow const *> &second) {
  std::pair<std::vector<defiRow const *>, std::vector<defiRow const *>> missing_rows;

  auto it1 = first.begin();
  auto it2 = second.begin();
  while (it1 != first.end() && it2 != second.end()) {
    int result = std::strcmp((*it1)->name(), (*it2)->name());
    if (result < 0) {
      missing_rows.second.push_back(*it1);
      ++it1;
    }
    else if (result > 0) {
      missing_rows.first.push_back(*it2);
      ++it2;
    }
    else {
      ++it1; ++it2;
    }
  }
  while (it1 != first.end()) {
    missing_rows.second.push_back(*it1);
    ++it1;
  }
  while (it2 != second.end()) {
    missing_rows.first.push_back(*it2);
    ++it2;
  }

  return missing_rows;
}

bool PlacementEvaluator::check_row_equivalence() {
  auto missing_rows_pair = compare_row_names(_sorted_initial_rows, _sorted_final_rows);
  bool missing_rows = false;
  if (!missing_rows_pair.first.empty()) {
    missing_rows = true;
    for (defiRow const *row : missing_rows_pair.first) {
      std::cout << "[!] Row " << std::string(row->name()) << " is missing from the initial placement.\n";
    }
  }
  if (!missing_rows_pair.second.empty()) {
    missing_rows = true;
    for (defiRow const *row : missing_rows_pair.second) {
      std::cout << "[!] Row " << std::string(row->name()) << " is missing from the final placement.\n";
    }
  }

  return !missing_rows;
}
