#include "arg_parser.h"

#include<boost/tokenizer.hpp>

int arg_parser::parse_options(int argc, char **argv) {
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help") > 0) {
    std::cout << desc << "\n";
    return 1;
  }

  if (vm.count("def") != 1) {
    std::cerr << "You must specify at least one def file.\n"
              << "You can specify an initial and final def files separating them with a semicolon.\n";
    return 2;
  }

  {
    boost::char_separator<char> sep(";");
    boost::tokenizer<boost::char_separator<char>> tokens(vm["def"].as<std::string>(), sep);
    for (std::string const &token : tokens) {
      defs.push_back(token);
    }
  }

  if (vm.count("lef") != 1) {
    std::cerr << "You must specify at least one lef file.\n"
              << "You can specify multiple lef files separating them with a semicolon.\n";
    return 3;
  }

  {
    boost::char_separator<char> sep(";");
    boost::tokenizer<boost::char_separator<char>> tokens(vm["lef"].as<std::string>(), sep);
    for (std::string const &token : tokens) {
      lefs.push_back(token);
    }
  }

  return 0;
}
